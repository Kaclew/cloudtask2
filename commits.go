package cloudtask2

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

//ProjectPage is the gitlab page, it's also used by languages.go
const ProjectPage = "https://git.gvk.idi.ntnu.no/api/v4/projects/"
const commitAddition = "/repository/commits?per_page=100"

//GetProjects simply gets projects with respect to the limit the user set
//If there are more than 100 entries, the "limit" variable can be different from actual limit.
func GetProjects(limit string, auth string) Repositories {
	var projects Repositories

	//This is a check to find out if private token has been used.
	//If it has not been used, proceed without it.
	URL := (ProjectPage + "?per_page=" + limit)
	if auth != "" {
		URL = (URL + "&private_token=" + auth)
	}

	//(gitlabpage)/projects?per_page=[{limit}&page=1]{&auth=token}
	//Square bracket = browsing through more than 100 entries.
	res, err := http.Get(URL)
	if err != nil {
		fmt.Println("Couldn't receive projects!")
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		err = json.Unmarshal(data, &projects)
	}

	return projects
}

//getCommitsd return the amount of commits a project has
func getCommits(id int) int {
	//Shows how many commits there are in total in a single project
	amountOfCommits := 0

	//Used to get the actual commits from the website
	var commits NumOfCommits

	//Number of commit "pages" over 100. If there are 200 commits, the number will
	//up to 2 in the for loop.
	numOfPages := 1

	//Goes through every commit page
	for i := 1; i == numOfPages; i++ {

		//(gitlabsite)/projects/{ID of repo}/repository/commits?per_page=100&page={i}
		res, err := http.Get(ProjectPage + strconv.Itoa(id) + commitAddition + "&page=" + strconv.Itoa(i))

		if err != nil {
			fmt.Println("Couldn't receive commits!")
		} else {
			data, _ := ioutil.ReadAll(res.Body)
			err = json.Unmarshal(data, &commits)
			//Adds amount of commits to variable
			amountOfCommits += len(commits)
		}
		//If there is there is more than 100 commits per page (full page)
		//itterate through another page
		if amountOfCommits == 100*i {
			numOfPages++
		}
	}

	return amountOfCommits
}

//addToFinal adds the data from the templates into the main strcut
func addToFinal(final *Commits, repo Repositories) {
	for i := 0; i < len(repo); i++ {
		var subfinal Repo

		subfinal.Path = repo[i].Path
		subfinal.Commits = getCommits(repo[i].ID)

		final.Repos = append(final.Repos, subfinal)
	}
}

//sorter sorts the results by commits.
//I know I could have used sort function.
func sorter(final Commits) Commits {
	for i := 0; i < len(final.Repos); i++ {
		for j := 0; j < len(final.Repos); j++ {
			if final.Repos[i].Commits > final.Repos[j].Commits {
				var temp TempCommit
				temp.Repos = final.Repos[i]

				final.Repos[i] = final.Repos[j]
				final.Repos[j] = temp.Repos
			}
		}
	}
	return final
}

//GetCommitTab returns the full commit section
func GetCommitTab(limit string, auth string) Commits {
	//Struct that is going to be shown to the user
	var final Commits

	//Structs that get data and will be sent to "final".
	var repos Repositories

	//String to int for caluclations
	limitInt, err := strconv.Atoi(limit)
	if err != nil {
		fmt.Println("This should never show up")
	}

	//An int to determine how many pages have to be checked
	var numOfPages = (limitInt) / 100

	//Int to check how many repos how many other repos there are
	//This is only used when a there is more than one page to search
	//For example: 412, it will look for remaining 12 repos.
	var numOfEntries = limitInt % 100

	//If the user wants more than 100 entries
	if numOfPages >= 1 {

		//Go through multiple pages if there are more than 200 pages
		//Otherwise go through 100. Add the results into final struct
		for i := 1; i <= numOfPages; i++ {
			pageNumberString := strconv.Itoa(i)

			repos = GetProjects("100&page="+pageNumberString, auth)
			addToFinal(&final, repos)
		}

		//Converts ints into strings so that functions can
		//look for the data.
		numOfPages++
		pageNumberString := strconv.Itoa(numOfPages)

		repos = GetProjects("100&page="+pageNumberString, auth)
		repos = repos[:len(repos)]
		if len(repos) > numOfEntries {
			repos = repos[:numOfEntries]
		}
		//Gets only the necessary slice that the user wanted. #look numOfEntries
		//And adds it to the final struct
		addToFinal(&final, repos)

	} else { //If there is less than 99 pages, get the amount the user wanted
		repos = GetProjects(limit, auth)
		addToFinal(&final, repos)
	}

	//Sorts whole array
	final = sorter(final)

	//Auth at the bottom of the struct is set if token was used.
	if auth != "" {
		final.Auth = "true"
	} else {
		final.Auth = "false"
	}

	return final
}
