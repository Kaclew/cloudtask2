package cloudtask2

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

//WebhookSender sends the get request to url.
func WebhookSender(action string, limit string) {
	for _, hook := range WebhookDB {

		if hook.Event == action {

			var WebhookInvo WebhookInvocation

			WebhookInvo.Event = hook.Event
			WebhookInvo.Time = time.Now()
			WebhookInvo.Params = limit

			URL := hook.URL

			data, _ := json.Marshal(WebhookInvo)

			_, err := http.Post(URL, "application/json", bytes.NewBuffer(data))
			if err != nil {
				fmt.Println("Bad request:" + err.Error())
			}
		}

	}
}

//Remover removes a hook that is unnecessary
func Remover(slice []WebhookRead, i int) []WebhookRead {
	slice[i] = slice[len(slice)-1]
	return slice[:len(slice)-1]
}
