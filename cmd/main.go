package main

import (
	"cloudtask2"
	"fmt"
	"log"
	"net/http"
	"os"
)

func handlerNil(wri http.ResponseWriter, req *http.Request) {
	http.Error(wri, "Invalid URL \nConsider using /repocheck/v1/(commits, language, hooks) "+
		"\nUse ?limit={int} for amount of commits or languages to be shown."+
		"\nWant more results? Use your private token with &auth={token}!"+
		"\nFor webhooks posts, be careful to write event in lower case."+
		"\nTo view webhooks, to \"get\" request to /hooks/, to register webhook"+
		"write {\"URL\" : \"[Your URL]\" , \"Event\" : \"[commits/status/language]\"}", http.StatusBadRequest)
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	//Api code : cDuvJAAsYTCtGHkJxsbV

	http.HandleFunc("/", handlerNil)
	http.HandleFunc("/repocheck/v1/commits", cloudtask2.HandlerCommit)
	http.HandleFunc("/repocheck/v1/language", cloudtask2.HandlerLanguages)
	http.HandleFunc("/repocheck/v1/hooks/", cloudtask2.HandlerHooks)

	fmt.Println("Currently working")
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
