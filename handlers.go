package cloudtask2

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

//WebhookDB comment
var WebhookDB []WebhookRead

//HandlerCommit takes care of handling all interaction with commits
func HandlerCommit(wri http.ResponseWriter, req *http.Request) {
	ioutil.ReadFile("Webhooks.json")

	http.Header.Add(wri.Header(), "Content-type", "application/json")

	var final Commits

	limit := req.FormValue("limit")
	token := req.FormValue("auth")

	if limit != "" && token != "" {
		final = GetCommitTab(limit, token)
	} else if limit != "" {
		final = GetCommitTab(limit, "")
	} else {
		final = GetCommitTab("5", "")
	}

	json.NewEncoder(wri).Encode(final)
	WebhookSender("commit", limit)
}

//HandlerLanguages handles everything related to languages
func HandlerLanguages(wri http.ResponseWriter, req *http.Request) {
	http.Header.Add(wri.Header(), "Content-type", "application/json")
	ioutil.ReadFile("Webhooks.json")

	var final LanguagesList
	var repo RepoName

	limit := req.FormValue("limit")
	token := req.FormValue("auth")

	//PAYLOAD
	json.NewDecoder(req.Body).Decode(&repo)

	if limit != "" && token != "" {
		final = GetLanguageTab(limit, token, repo)
	} else if limit != "" {
		final = GetLanguageTab(limit, "", repo)
	} else {
		final = GetLanguageTab("5", "", repo)
	}

	json.NewEncoder(wri).Encode(final)

	WebhookSender("language", limit)

}

//HandlerHooks handles hooks
func HandlerHooks(wri http.ResponseWriter, req *http.Request) {
	ioutil.ReadFile("Webhooks.json")

	parts := strings.Split(req.URL.Path, "/")
	WebhookDB = readFromFile(WebhookDB)

	switch req.Method {
	case http.MethodPost:
		var webhook WebhookRead

		json.NewDecoder(req.Body).Decode(&webhook)

		switch webhook.Event {
		case "commit", "language", "status":
			webhook.Time = time.Now()

			highestNum := 0
			for i := 0; i < len(WebhookDB); i++ {
				if WebhookDB[i].ID > highestNum {
					highestNum = WebhookDB[i].ID
				}
			}
			webhook.ID = highestNum + 1

			WebhookDB = append(WebhookDB, webhook)
			fmt.Fprintln(wri, webhook.ID)

			data, _ := json.Marshal(WebhookDB)
			ioutil.WriteFile("Webhooks.json", data, 0644)

		default:
			http.Error(wri, "Your event has to be \"commit\", \"language\" or \"status\"", 400)
		}

	case http.MethodGet:
		http.Header.Add(wri.Header(), "Content-type", "application/json")

		userID, _ := strconv.Atoi(parts[4])

		ioutil.ReadFile("Webhooks.json")

		if parts[4] != "" {
			for i := 0; i < len(WebhookDB); i++ {
				if WebhookDB[i].ID == userID {
					json.NewEncoder(wri).Encode(WebhookDB[userID])
				} else {
					http.Error(wri, "This ID does not exist!: ", http.StatusNotFound)
				}
			}
		} else {
			err := json.NewEncoder(wri).Encode(WebhookDB)
			if err != nil {
				http.Error(wri, "Something went wrong: "+err.Error(), http.StatusInternalServerError)
			}
		}

	case http.MethodDelete:
		deleteID, err := strconv.Atoi(parts[4])

		if err != nil {
			http.Error(wri, "Could not receive what id to delete!", http.StatusNotFound)
		}

		WebhookDB = Remover(WebhookDB, deleteID)
		fmt.Fprintln(wri, "Deleted ID: "+parts[4])

		data, _ := json.Marshal(WebhookDB)
		ioutil.WriteFile("Webhooks.json", data, 0644)

	default:
		http.Error(wri, "Invalid method "+req.Method, http.StatusBadRequest)
	}
}

func readFromFile(db []WebhookRead) []WebhookRead {
	jsonFile, _ := os.Open("Webhooks.json")
	val, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(val, &db)
	return db
}
