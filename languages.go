package cloudtask2

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
)

//getLanguage retrives languages used in a project and puts the results into a map.
func getLanguage(id string, auth string, langs map[string]int) {
	//Used to get names of languages
	tempMap := make(map[string]float64)

	res, err := http.Get(ProjectPage + id + "/languages?private_token=" + auth)
	if err != nil {
		fmt.Println("Could not receive languages")
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		json.Unmarshal(data, &tempMap)
	}

	//For each language, put it into map that will keep count of occurrences
	for key := range tempMap {
		langs[key]++
	}

}

//Inserts information from sub struct into the user readable struct
func addToLangList(limit string, lang []Languages, langList *LanguagesList) {
	//Used to limit amount of results
	var temp []Languages

	//Limit has to be changed to int
	limitInt, _ := strconv.Atoi(limit)

	//Limits the results. If there are fewer languages than limit, reduce limit to amount of langs
	if limitInt > len(lang) {
		temp = lang[:len(lang)]
	} else {
		temp = lang[:limitInt]
	}

	//Add everything from the limited struct into the main one.
	for i := 0; i < len(temp); i++ {
		langList.Languages = append(langList.Languages, temp[i].Language)
	}
}

//GetLanguageTab  gets all the languages and shows them in human readable format
func GetLanguageTab(limit string, auth string, repoPayload RepoName) LanguagesList {
	//Main struct that will be returned
	var langList LanguagesList

	//Sub struct to sort results and give appropriate amount
	var lang []Languages

	//Used to count languagues in repos
	langs := make(map[string]int)

	//Checks the amount of pages of repso
	pagesOfRepos := 1

	//Gets all repos on the site.
	for i := 1; i <= pagesOfRepos; i++ {

		var repo Repositories
		repo = GetProjects("100&page="+strconv.Itoa(i), auth)

		//Gets all languages for each repo sends them to langs map.
		for j := 0; j < len(repo); j++ {

			//PAYLOAD: If payload is not empty
			if len(repoPayload.Name) > 0 {

				//Look for repos with same name and add them to langs map
				for k := 0; k < len(repoPayload.Name); k++ {

					if repo[j].Name == repoPayload.Name[k] {
						getLanguage(strconv.Itoa(repo[j].ID), auth, langs)
					}
				}

				//WITHOUT PAYLOAD:
			} else {
				getLanguage(strconv.Itoa(repo[j].ID), auth, langs)

			}
		}
		if len(repo) == 100 {
			pagesOfRepos++
		}
	}

	//Inserts all languages into a struct to sort them
	for lan, amo := range langs {
		lang = append(lang, Languages{lan, amo})
	}

	//Sorts the struct
	sort.Slice(lang, func(i, j int) bool {
		return lang[i].Amount > lang[j].Amount
	})

	//Puts it into struct that user will read. Will display {limit} amount of languages
	addToLangList(limit, lang, &langList)

	//Checks if private token was used.
	if auth != "" {
		langList.Auth = "true"
	} else {
		langList.Auth = "false"
	}

	return langList
}
