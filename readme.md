1. While working on the project, I had a malfunction that prevented me from finishing it.
2. The application is not deployed on Openstack, it's deployed on Heroku like previous assignment.
3. Status is not implemented.
4. The webhooks are not stored in external database like Firebase or MongoDB, they are saved locally.
5. The files are actually not saved at all since this version did not readFromFile function that actually read the files.
6. Webhooks are uncommented.
7. To access webhook page, you have to parse "repocheck/v1/hooks/" with "/" at the end, otherwise it won't work.
