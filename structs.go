package cloudtask2

import "time"

// ******************* Commit structs ********************* //

//Commits is the most important struct as it glues together all
//results into a single struct. It uses Repo struct to give details
//about repositories.
type Commits struct {
	Repos []Repo `json:"repos"`
	Auth  string `json:"auth"`
}

//Repo struct contains all the necessary data for showing the
//information that user is looking for.
type Repo struct {
	Path    string `json:"path_with_namespace"`
	Commits int    `json:"commits"`
}

//Repositories struct is used for finding repositories from
//the main site and is not showed to the user. Id is used
//by one of the functions to find the amount of number of commits
//and path is sent to Repo struct.
type Repositories []struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Path string `json:"path_with_namespace"`
}

//NumOfCommits fucntion is  this struct is to check how many
//commits a repository has made. Id is not used anywhere.
type NumOfCommits []struct {
	ID string `json:"id"`
}

//TempCommit stores the value of Commit
//struct. This struct is used only while sorting
type TempCommit struct {
	Repos Repo `jsonrepository:"repos"`
}

// ******************* Language structs ******************* //

//LanguagesList shows all information about languages.
type LanguagesList struct {
	Languages []string `json:"languagues"`
	Auth      string   `json:"auth"`
}

//Languages struct is used to sort the map and to input the results
//into the LanguaguesList
type Languages struct {
	Language string
	Amount   int
}

//RepoName is used only when doing the payload request
type RepoName struct {
	Name []string
}

// ******************* Webhook structs ******************* //

//WebhookRead comment
type WebhookRead struct {
	ID    int       `json:"id"`
	Event string    `json:"event"`
	URL   string    `json:"url"`
	Time  time.Time `json:"time"`
}

//WebhookInvocation comment
type WebhookInvocation struct {
	Event  string    `json:"event"`
	Params string    `json:"params"`
	Time   time.Time `json:"time"`
}
